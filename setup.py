from setuptools import setup

setup(
    name='python-smsc',
    version='1.7.0',
    url='http://bitbucket.org/medsafe/python-smsc/',
    description=(
        'Python packaging for SMSC.RU python API bindings.'
    ),
    author='Andrey Mozgunov',
    author_email='andrey.mozgunov@softline.ru',
    packages=[
        'smsc',
    ]
)
