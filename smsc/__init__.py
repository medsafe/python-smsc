# encoding: utf-8
from smsc_api import *


class _SMSC(SMSC):

    def __init__(self, login, password):
        global SMSC_LOGIN
        global SMSC_PASSWORD
        SMSC_LOGIN = login
        SMSC_PASSWORD = password

    # One to one copy/pase from sources.  Fixes login and password visibility.
    def _smsc_send_cmd(self, cmd, arg=""):
        url = ifs(SMSC_HTTPS, "https", "http") + "://smsc.ru/sys/" + cmd + ".php"
        arg = "login=" + quote(SMSC_LOGIN) + "&psw=" + quote(SMSC_PASSWORD) + "&fmt=1&charset=" + SMSC_CHARSET + "&" + arg

        i = 0
        ret = ""

        while ret == "" and i < 3:
            if i > 0:
                sleep(2)

            if SMSC_POST or len(arg) > 2000:
                data = urlopen(url, arg.encode(SMSC_CHARSET))
            else:
                data = urlopen(url + "?" + arg)

            ret = str(data.read())

            i += 1

        if ret == "":
            if SMSC_DEBUG:
                print("Ошибка чтения адреса: " + url)
            ret = "," # фиктивный ответ

        return ret.split(",")

SMSC = _SMSC
